package com.amrutha.todo.serviceImpl;


import com.amrutha.todo.dto.TodoListResponseDto;
import com.amrutha.todo.entity.TodoList;
import com.amrutha.todo.repository.TodoListRepository;
import com.amrutha.todo.service.TodoListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TodoListServiceImpl implements TodoListService {

    @Autowired
    TodoListRepository todoListRepository;

    @Override
    public TodoListResponseDto addItem(String item) {

        TodoListResponseDto todoListResponseDto = new TodoListResponseDto();
        List<TodoList> todoLists = new ArrayList<>();
        TodoList newItem = new TodoList(item);
        if (item != null && !item.equals("")) {
            todoListRepository.save(newItem);
            todoLists.add(newItem);
            todoListResponseDto.setItems(todoLists);
            todoListResponseDto.setStatusCode(HttpStatus.CREATED);
            todoListResponseDto.setMessage("Item Added Succesfully!!");
            return todoListResponseDto;
        } else {
            todoListResponseDto.setStatusCode(HttpStatus.FORBIDDEN);
            todoListResponseDto.setMessage("Please fill in the required filed");
            return todoListResponseDto;
        }
    }

    @Override
    public TodoListResponseDto getItems() {
        List<TodoList> results = todoListRepository.findAll();
        TodoListResponseDto todoListResponseDto = new TodoListResponseDto();
        todoListResponseDto.setItems(results);
        if (results == null || results.size() == 0) {
            todoListResponseDto.setMessage("No Items created yet!");
        } else {
            todoListResponseDto.setMessage("Listing all items.");
        }
        todoListResponseDto.setStatusCode(HttpStatus.OK);
        return todoListResponseDto;
    }
}
