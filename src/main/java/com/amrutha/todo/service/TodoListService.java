package com.amrutha.todo.service;


import com.amrutha.todo.dto.TodoListResponseDto;

public interface TodoListService {

    public TodoListResponseDto addItem(String item);

    public TodoListResponseDto getItems();
}
