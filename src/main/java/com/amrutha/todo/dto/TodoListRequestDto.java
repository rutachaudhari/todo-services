package com.amrutha.todo.dto;

public class TodoListRequestDto {

    private String item;

    public TodoListRequestDto() {

    }

    public TodoListRequestDto(String item) {
        this.item = item;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }
}
