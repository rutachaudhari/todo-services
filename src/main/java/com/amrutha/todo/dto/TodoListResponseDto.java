package com.amrutha.todo.dto;

import com.amrutha.todo.entity.TodoList;
import org.springframework.http.HttpStatus;

import java.util.List;

public class TodoListResponseDto {

    private HttpStatus statusCode;
    private List<TodoList> items;
    private String message;

    public TodoListResponseDto() {

    }

    public HttpStatus getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(HttpStatus statusCode) {
        this.statusCode = statusCode;
    }

    public List<TodoList> getItems() {
        return items;
    }

    public void setItems(List<TodoList> items) {
        this.items = items;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
