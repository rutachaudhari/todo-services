package com.amrutha.todo.controller;

import com.amrutha.todo.dto.TodoListRequestDto;
import com.amrutha.todo.dto.TodoListResponseDto;
import com.amrutha.todo.service.TodoListService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@EnableSwagger2
@ApiOperation("Todo list")
@RestController
public class TodoListController {

    @Autowired
    TodoListService todoListService;

    @RequestMapping(value = "/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TodoListResponseDto> addItem(@RequestBody TodoListRequestDto todoListRequestDto) {
        TodoListResponseDto todoListResponseDto = todoListService.addItem(todoListRequestDto.getItem());
        return new ResponseEntity<>(todoListResponseDto, todoListResponseDto.getStatusCode());
    }

    @GetMapping("/list")
    public ResponseEntity<TodoListResponseDto> getAllItems(HttpServletRequest req, HttpServletResponse httpServletResponse) {
        TodoListResponseDto todoListResponseDto = todoListService.getItems();
        return new ResponseEntity<>(todoListResponseDto, todoListResponseDto.getStatusCode());
    }
}
