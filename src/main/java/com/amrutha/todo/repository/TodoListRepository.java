package com.amrutha.todo.repository;

import com.amrutha.todo.entity.TodoList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface TodoListRepository extends JpaRepository<TodoList, Integer> {

//    @Modifying
//    @Query(value = "insert into todo (item) VALUES (:item, :checkbox)", nativeQuery = true)
//    @Transactional
//    void updateTheList(@Param("item") String item, @Param("checkbox") Boolean checkbox);

}
