package com.amrutha.todo.entity;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

import static java.lang.Boolean.*;

@Entity
@Table(name = "todo")
public class TodoList implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @NotNull
    @Column(name = "item")
    private String item;

    @Column(name = "checkbox")
    private Boolean checkbox = false;

    public TodoList(@NotNull String item) {
        this.item = item;
        this.checkbox = false;
    }

    public TodoList() {
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public Boolean getCheckbox() {
        return checkbox;
    }

    public void setCheckbox(Boolean checkbox) {
        this.checkbox = checkbox;
    }
}
