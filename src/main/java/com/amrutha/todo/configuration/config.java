package com.amrutha.todo.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

@Configuration
@EnableConfigurationProperties(DataSourceProperties.class)
@EnableTransactionManagement
public class config {

    private static final Logger logger = LoggerFactory.getLogger(config.class);

    @Autowired
    Environment env;

    @Autowired
    ApplicationContext appContext;

    @Autowired
    private DataSourceProperties properties;


    @Bean(name = "datasourceForTodo")
    @ConfigurationProperties("postgres.datasource")
    @Primary
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(env.getProperty("postgres.datasource.driver-class-name"));
        dataSource.setUrl(env.getProperty("postgres.datasource.url"));
        dataSource.setUsername(env.getProperty("postgres.datasource.username")); // get from PV
        dataSource.setPassword(env.getProperty("postgres.datasource.password")); // get from PV

        return dataSource;
    }

    @Bean(name = "txManager1")
    @Autowired
    @Primary
    PlatformTransactionManager txManager1(@Qualifier("datasourceForTodo") DataSource datasource) {
        return new DataSourceTransactionManager(datasource);
    }
}
